#include <stdio.h>
#include <iostream>
using namespace std;

//game includes
#include <time.h>
#include "Game.h"

int main(int argc, char *argv[]){
    srand(time(NULL));

    // setup Panda3d
    PandaFramework pandaFramework;
    pandaFramework.open_framework(argc, argv);
    PT(WindowFramework) windowFrameworkPtr = pandaFramework.open_window();
    if(windowFrameworkPtr == NULL)
        {
            nout << "ERROR: could not open the WindowFramework." << endl;
            return 1; // error
        }
    // Create an instance of our class
    Game game(windowFrameworkPtr);

    // Run the simulation
    pandaFramework.main_loop();

    // quit Panda3d
    pandaFramework.close_framework();
    return 0;
}
