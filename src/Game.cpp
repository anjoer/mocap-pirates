#include "Game.h"

Game::Game(WindowFramework* p_ptrWindowFramework)
: m_ptrWindowFramework(p_ptrWindowFramework){

    //initialize number of game rounds
    m_iRoundCounter = 10;


    //check if m_ptrWindowFramework not null otherwise return error
    if(m_ptrWindowFramework == NULL){
        nout << "ERROR: Game::Game() m_ptrWindowFramwork not initialized!";
        return;
    }

    //setup a taskmanager and a task for function callPlayGame
    PT(GenericAsyncTask) ptrTask = new GenericAsyncTask("playGame",callPlayGame,this);
    if(ptrTask != NULL){
        AsyncTaskManager::get_global_ptr()->add(ptrTask);
    } else {
        nout << "ERROR: Game::Game() ptrTask not initialized!";
        return;
    }
    // setup the state machine
    m_eState = NEWGAME;

    // call initialisation methodes
    setupActors();
    setupAssets();
    setupKeyEvent();
    setupScenes();
    setupOnScreenText();

    // setup camera and default lights
    m_Camera = m_ptrWindowFramework->get_camera_group();

    ///TODO: Kamera sperren
    // m_ptrWindowFramework->setup_trackball();


    // setup the World
    m_ptrWindowFramework->set_background_type(WindowFramework::BT_black);

    m_ModelsNp = m_ptrWindowFramework->get_panda_framework()->get_models();
    m_Environment = m_ptrWindowFramework->load_model(m_ModelsNp, "../../models/welt");
    m_ptrRenderNp = m_ptrWindowFramework->get_render();
    m_Environment.reparent_to(m_ptrRenderNp);
    m_Environment.set_pos(50,50,0);

    m_vAssets["BuehneStrand"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_buehne");
    m_vAssets["BuehneStrand"].set_pos(50, 160, 0);
    m_vAssets["BuehneStrand"].set_scale(3.);
    m_vAssets["BuehneStrand"].set_h(90);
 //   m_vAssets["Buehne"].reparent_to(m_ptrRenderNp);

    m_vAssets["BuehneStadt"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_buehne_stadt");
    m_vAssets["BuehneStadt"].set_pos(50, 160, 0);
    m_vAssets["BuehneStadt"].set_scale(3.);
 //   m_vAssets["BuehneStadt"].reparent_to(m_ptrRenderNp);

    m_vAssets["BuehneSchiff"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_buehne_schiff");
    m_vAssets["BuehneSchiff"].set_pos(50, 160, 0);
    m_vAssets["BuehneSchiff"].set_scale(1);
 //   m_vAssets["BuehneSchiff"].set_h(60);

    m_PlayerModel = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/player");
    m_PlayerModel.set_scale(5);
    m_PlayerModel.set_color(0,0,0,1);
    m_PlayerModel.reparent_to(m_ptrRenderNp);

    ///TODO: Thief model
//    m_ThiefModel = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/player");
//    m_ThiefModel.set_scale(5);
//    m_ThiefModel.set_color(255,255,255,1);
//    m_ThiefModel.reparent_to(m_ptrRenderNp);
}

Game::~Game()
{
    //dtor
}

void Game::setupActors(){

    //Schlägerei
    CActor::AnimMap* animationMapFight;
    m_vActors["Fight"] = CActor();
    animationMapFight = new CActor::AnimMap();
    (*animationMapFight)["../../models/sz_schlaegerei"].push_back("fight");
    m_vActors["Fight"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_schlaegerei",
                        animationMapFight,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Fight"].reparent_to(m_ptrRenderNp);

    //Market
    CActor::AnimMap* animationMapMarket;
    m_vActors["Market"] = CActor();
    animationMapMarket = new CActor::AnimMap();
    (*animationMapMarket)["../../models/sz_marktkauf_anim"].push_back("market");
    m_vActors["Market"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_marktkauf",
                        animationMapMarket,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Market"].reparent_to(m_ptrRenderNp);

    //OneDrunken
    CActor::AnimMap* animationMapOneDrunken;
    m_vActors["OneDrunken"] = CActor();
    animationMapOneDrunken = new CActor::AnimMap();
    (*animationMapOneDrunken)["../../models/sz_OneDrunken"].push_back("OneDrunken");
    m_vActors["OneDrunken"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_OneDrunken",
                        animationMapOneDrunken,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["OneDrunken"].reparent_to(m_ptrRenderNp);

    //Gitter
    CActor::AnimMap* animationMapGitter;
    m_vActors["Gitter"] = CActor();
    animationMapGitter = new CActor::AnimMap();
    (*animationMapGitter)["../../models/sz_Gitter"].push_back("Gitter");
    m_vActors["Gitter"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_Gitter",
                        animationMapGitter,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Gitter"].reparent_to(m_ptrRenderNp);

    //Kapitaen
    CActor::AnimMap* animationMapKapitaen;
    m_vActors["Kapitaen"] = CActor();
    animationMapKapitaen = new CActor::AnimMap();
    (*animationMapKapitaen)["../../models/sz_Kapitaen"].push_back("Kapitaen");
    m_vActors["Kapitaen"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_Kapitaen",
                        animationMapKapitaen,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Kapitaen"].reparent_to(m_ptrRenderNp);

    //Marschieren
    CActor::AnimMap* animationMapMarschieren;
    m_vActors["Marschieren"] = CActor();
    animationMapMarschieren = new CActor::AnimMap();
    (*animationMapMarschieren)["../../models/sz_Marschieren"].push_back("Marschieren");
    m_vActors["Marschieren"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_Marschieren",
                        animationMapMarschieren,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Marschieren"].reparent_to(m_ptrRenderNp);

    //Drunk
    CActor::AnimMap* animationMapDrunk;
    m_vActors["Drunk"] = CActor();
    animationMapDrunk = new CActor::AnimMap();
    (*animationMapDrunk)["../../models/sz_betrunken"].push_back("Drunk");
    m_vActors["Drunk"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_betrunken",
                        animationMapDrunk,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Drunk"].reparent_to(m_ptrRenderNp);

    //Rühren
    CActor::AnimMap* animationMapRuehren;
    m_vActors["Ruehren"] = CActor();
    animationMapRuehren = new CActor::AnimMap();
    (*animationMapRuehren)["../../models/sz_ruehren"].push_back("Ruehren");
    m_vActors["Ruehren"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_ruehren",
                        animationMapRuehren,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Ruehren"].reparent_to(m_ptrRenderNp);

    //Rühren
    CActor::AnimMap* animationMapGeist;
    m_vActors["Geist"] = CActor();
    animationMapGeist = new CActor::AnimMap();
    (*animationMapGeist)["../../models/sz_geist"].push_back("Geist");
    m_vActors["Geist"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_geist",
                        animationMapGeist,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Geist"].reparent_to(m_ptrRenderNp);

    //Sandburg
    CActor::AnimMap* animationMapSandburg;
    m_vActors["Sandburg"] = CActor();
    animationMapSandburg = new CActor::AnimMap();
    (*animationMapSandburg)["../../models/sz_sandburgbauen"].push_back("Sandburg");
    m_vActors["Sandburg"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_sandburgbauen",
                        animationMapSandburg,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Sandburg"].reparent_to(m_ptrRenderNp);

    //Klatschen
    CActor::AnimMap* animationMapClap;
    m_vActors["Clap"] = CActor();
    animationMapClap = new CActor::AnimMap();
    (*animationMapClap)["../../models/sz_klatschen"].push_back("Clap");
    m_vActors["Clap"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_klatschen",
                        animationMapClap,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Clap"].reparent_to(m_ptrRenderNp);

    //Steuerman
    CActor::AnimMap* animationMapSkipper;
    m_vActors["Skipper"] = CActor();
    animationMapSkipper = new CActor::AnimMap();
    (*animationMapSkipper)["../../models/sz_steuermann"].push_back("Skipper");
    m_vActors["Skipper"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_steuermann",
                        animationMapSkipper,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Skipper"].reparent_to(m_ptrRenderNp);

    //Surfen
    CActor::AnimMap* animationMapSurfing;
    m_vActors["Surfing"] = CActor();
    animationMapSurfing = new CActor::AnimMap();
    (*animationMapSurfing)["../../models/sz_surfen"].push_back("Surfing");
    m_vActors["Surfing"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_surfen",
                        animationMapSurfing,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Surfing"].reparent_to(m_ptrRenderNp);

    //Mast klettern
    CActor::AnimMap* animationMapMastclimb;
    m_vActors["Mastclimb"] = CActor();
    animationMapMastclimb = new CActor::AnimMap();
    (*animationMapMastclimb)["../../models/sz_mastklettern"].push_back("Mastclimb");
    m_vActors["Mastclimb"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_mastklettern",
                        animationMapMastclimb,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Mastclimb"].reparent_to(m_ptrRenderNp);

    //Hüpfen
    CActor::AnimMap* animationMapHop;
    m_vActors["Hop"] = CActor();
    animationMapHop = new CActor::AnimMap();
    (*animationMapHop)["../../models/sz_huepfen"].push_back("Hop");
    m_vActors["Hop"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_huepfen",
                        animationMapHop,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Hop"].reparent_to(m_ptrRenderNp);


    //Tanzen
    CActor::AnimMap* animationMapDance;
    m_vActors["Dance"] = CActor();
    animationMapDance = new CActor::AnimMap();
    (*animationMapDance)["../../models/sz_tanz"].push_back("Dance");
    m_vActors["Dance"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_tanz",
                        animationMapDance,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Dance"].reparent_to(m_ptrRenderNp);

    //Tanzen
    CActor::AnimMap* animationMapLookout;
    m_vActors["Lookout"] = CActor();
    animationMapLookout = new CActor::AnimMap();
    (*animationMapLookout)["../../models/sz_aussicht"].push_back("Lookout");
    m_vActors["Lookout"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_aussicht",
                        animationMapLookout,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Lookout"].reparent_to(m_ptrRenderNp);

    //Schaufeln
    CActor::AnimMap* animationMapShovel;
    m_vActors["Shovel"] = CActor();
    animationMapShovel = new CActor::AnimMap();
    (*animationMapShovel)["../../models/sz_schaufeln"].push_back("Shovel");
    m_vActors["Shovel"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_schaufeln",
                        animationMapShovel,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Shovel"].reparent_to(m_ptrRenderNp);

    //Sägen
    CActor::AnimMap* animationMapSaw;
    m_vActors["Saw"] = CActor();
    animationMapSaw = new CActor::AnimMap();
    (*animationMapSaw)["../../models/sz_saegen"].push_back("Saw");
    m_vActors["Saw"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_saegen",
                        animationMapSaw,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Saw"].reparent_to(m_ptrRenderNp);

    //Schwertkampf
    CActor::AnimMap* animationMapSwordfight;
    m_vActors["Swordfight"] = CActor();
    animationMapSwordfight = new CActor::AnimMap();
    (*animationMapSwordfight)["../../models/sz_schwertkampf"].push_back("Swordfight");
    m_vActors["Swordfight"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_schwertkampf",
                        animationMapSwordfight,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Swordfight"].reparent_to(m_ptrRenderNp);

    //Prostiuierte
    CActor::AnimMap* animationMapProstitute;
    m_vActors["Prostitute"] = CActor();
    animationMapProstitute = new CActor::AnimMap();
    (*animationMapProstitute)["../../models/sz_prostituierte"].push_back("Prostitute");
    m_vActors["Prostitute"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_prostituierte",
                        animationMapProstitute,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Prostitute"].reparent_to(m_ptrRenderNp);

    //Selbstmord
    CActor::AnimMap* animationMapSelfkill;
    m_vActors["Selfkill"] = CActor();
    animationMapSelfkill = new CActor::AnimMap();
    (*animationMapSelfkill)["../../models/sz_selbstmord"].push_back("Selfkill");
    m_vActors["Selfkill"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_selbstmord",
                        animationMapSelfkill,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Selfkill"].reparent_to(m_ptrRenderNp);

    //Handstand
    CActor::AnimMap* animationMapHandstand;
    m_vActors["Handstand"] = CActor();
    animationMapHandstand = new CActor::AnimMap();
    (*animationMapHandstand)["../../models/sz_handstand"].push_back("Handstand");
    m_vActors["Handstand"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_handstand",
                        animationMapHandstand,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Handstand"].reparent_to(m_ptrRenderNp);

    //Angelsn
    CActor::AnimMap* animationMapAngeln;
    m_vActors["Angeln"] = CActor();
    animationMapAngeln = new CActor::AnimMap();
    (*animationMapAngeln)["../../models/sz_angeln"].push_back("Angeln");
    m_vActors["Angeln"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_angeln",
                        animationMapAngeln,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Angeln"].reparent_to(m_ptrRenderNp);

    //Cannon
    CActor::AnimMap* animationMapCannon;
    m_vActors["Cannon"] = CActor();
    animationMapCannon = new CActor::AnimMap();
    (*animationMapCannon)["../../models/sz_kanone"].push_back("Cannon");
    m_vActors["Cannon"].load_actor(m_ptrWindowFramework,
                        "../../models/sz_kanone",
                        animationMapCannon,
                        PartGroup::HMF_ok_wrong_root_name|
                        PartGroup::HMF_ok_anim_extra|
                        PartGroup::HMF_ok_part_extra);
    m_vActors["Cannon"].reparent_to(m_ptrRenderNp);
}

void Game::setupAssets(){
    m_vAssets["Angel"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_angel");
    m_vAssets["Axt"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_axt");
    m_vAssets["Faecher"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_faecher");
    m_vAssets["Hacke"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_hacke");
    m_vAssets["Loeffel"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_loeffel");
    m_vAssets["Lore"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_lore");
    m_vAssets["Messer"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_messer");
    m_vAssets["Plattform"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_plattform");
    m_vAssets["Rohling"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_rohling");
    m_vAssets["Saege"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_saege");
    m_vAssets["Schwert01"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_schwert");
    m_vAssets["Schwert02"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_schwert");
    m_vAssets["Steuerrad"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_steuerrad");
    m_vAssets["Topf"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_topf");
    m_vAssets["Kopf"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_ghost_head");
    m_vAssets["Marktstand"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_marktstand");
    m_vAssets["Angel"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_angel");
    m_vAssets["Gitter"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_gitter");
    m_vAssets["Stamm"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_stamm");
    m_vAssets["Stamm1"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_stamm");
    m_vAssets["Schaufel"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_schaufel");
    m_vAssets["Krug"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_krug");
    m_vAssets["Pistole"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_pistole");
    // add gitter
    // m_vAssets["Kopf"] = m_ptrWindowFramework->load_model(m_ModelsNp,"../../models/a_ghost_head");
}

void Game::setupScenes(){

    m_vScenes[sz_Kapitaen] = new Scene_kapitaen(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Gitter] = new Scene_Gitter(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_drunk] = new Scene_drunk(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_fight] = new Scene_fight(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_market] = new Scene_market(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_OneDrunken] = new Scene_oneDrunken(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Marschieren] = new Scene_marschieren(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Ruehren] = new Scene_ruehren(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Geist] = new Scene_geist(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Sandburg] = new Scene_sandburg(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Clap] = new Scene_clap(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Skipper] = new Scene_skipper(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Surfing] = new Scene_surfing(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Mastclimb] = new Scene_mastclimb(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Hop] = new Scene_hop(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Dance] = new Scene_dance(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Lookout] = new Scene_lookout(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Shovel] = new Scene_shovel(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Saw] = new Scene_saw(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Swordfight] = new Scene_swordfight(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Prostitute] = new Scene_prostitute(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Selfkill] = new Scene_selfkill(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Handstand] = new Scene_handstand(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_cannon] = new Scene_cannon(&m_vActors,&m_vAssets,&m_ptrRenderNp);
    m_vScenes[sz_Angeln] = new Scene_angeln(&m_vActors,&m_vAssets,&m_ptrRenderNp);


}

void Game::setupKeyEvent(){
    m_vKeymap.resize(K_EndKey); // K_keys is the last element of enum Key
    m_vKeymap[K_left     ] = false;
    m_vKeymap[K_right    ] = false;
    m_vKeymap[K_forward  ] = false;
    m_vKeymap[K_down     ] = false;
    m_vKeymap[K_s_startGame ] = false;
    m_vKeymap[K_i_info] = false;
    m_vKeymap[K_c_credits] = false;
    m_vKeymap[K_b_back] = false;

    m_ptrWindowFramework->enable_keyboard();
    m_ptrWindowFramework->get_panda_framework()->define_key("escape"        , "Quit"       , sys_exit                           , NULL);
    m_ptrWindowFramework->get_panda_framework()->define_key("arrow_left"    , "left"       , callSetKey<K_left          , true >, this);
    m_ptrWindowFramework->get_panda_framework()->define_key("arrow_right"   , "right"      , callSetKey<K_right         , true >, this);
    m_ptrWindowFramework->get_panda_framework()->define_key("arrow_up"      , "forward"    , callSetKey<K_forward       , true >, this);
    m_ptrWindowFramework->get_panda_framework()->define_key("arrow_down"    , "down"       , callSetKey<K_down          , true >, this);
    m_ptrWindowFramework->get_panda_framework()->define_key("s"             , "s_startGame", callSetKey<K_s_startGame   , true >, this);
    m_ptrWindowFramework->get_panda_framework()->define_key("i"             , "info"       , callSetKey<K_i_info,        true >, this);
    m_ptrWindowFramework->get_panda_framework()->define_key("c"             , "credits"    , callSetKey<K_c_credits,      true >, this);
    m_ptrWindowFramework->get_panda_framework()->define_key("b"             , "back"       , callSetKey<K_b_back,        true >, this);



}

void Game::setupOnScreenText(){
    // setup Titel. For text use this int codes in constructor
//            enum TextStyle
//            {
//            TS_plain = 1,
//            TS_screen_title,
//            TS_screen_prompt,
//            TS_name_confirm,
//            TS_black_on_white
//            };
    // set up the InfoText at the right bottom corner
    m_ptrInfoText = new COnscreenText("InfoText", 3);
    m_ptrInfoText->set_text("Piraten Insel");
    m_ptrInfoText->set_fg(LColorf(1,1,1,1));
    m_ptrInfoText->set_pos(LVecBase2f(1.3,-0.95));
    m_ptrInfoText->set_align(TextNode::A_right);
    m_ptrInfoText->set_scale(0.15);
    m_ptrInfoText->reparent_to(m_ptrWindowFramework->get_aspect_2d());

    // set up the menu
    m_ptrMenuText = new COnscreenText("MenuText", 5);
    m_ptrMenuText->set_text(m_sMenuText);
    m_ptrMenuText->set_fg(LColorf(1,1,1,1));
    m_ptrMenuText->set_pos(-1.5,0.8);
    m_ptrMenuText->set_align(TextNode::A_left);
    m_ptrMenuText->set_scale(0.059);
    m_ptrMenuText->reparent_to(m_ptrWindowFramework->get_aspect_2d());

    m_ptrTextBox = new COnscreenText("Textbox", 5);
    m_ptrTextBox->set_text("");
    m_ptrTextBox->set_fg(LColorf(1,1,1,1));
    m_ptrTextBox->set_pos(-.5,0.8);
    m_ptrTextBox->set_wordwrap(40.0);
    m_ptrTextBox->set_align(TextNode::A_left);
    m_ptrTextBox->set_scale(0.05);
}

bool Game::checkValidMove(Player* p_ptrPlayer,int p_iX, int p_iY,int p_iDiceValue){
    bool xRetVal = false;
    int iTmpX, iTmpY;
    bool xOK,yOK = false;
    //
    Position* ptrCurrentPos = p_ptrPlayer->getPosition();

    if(p_iX>-1 && p_iX<10){
        if(p_iX>ptrCurrentPos->getPositionX()){
            iTmpX = p_iX - ptrCurrentPos->getPositionX();
            xOK = true;

        } else {
            iTmpX = ptrCurrentPos->getPositionX() - p_iX;
            xOK = true;
        }
    }
    if(p_iY>-1 && p_iY<10){
        if(p_iY> ptrCurrentPos->getPositionY()){
            iTmpY = p_iY - ptrCurrentPos->getPositionY();
            yOK = true;
        } else {
            iTmpY = ptrCurrentPos->getPositionY() - p_iY;
            yOK = true;
        }
    }
    if(xOK && yOK){
        int iTmpSum;
        iTmpSum = iTmpY + iTmpX;
        if(iTmpSum <= p_iDiceValue){
            xRetVal = true;
        }
    }
//    if(xRetVal == true){
//        m_iRoundCounter--;
//    }
    if(xRetVal == false){
        cout << "############################# \n";
        cout << "Invalid Move! Try again! \n";
        cout << "############################# \n";
        m_ptrInfoText->clear_text();
        m_ptrInfoText->set_text("Wrong Move! Try again!");
    }
    return xRetVal;
}

bool Game::checkEndOfGame(Player* p_ptrPlayer, Thief* p_ptrThief){
bool xRetVal = false;

    if(p_ptrPlayer->getPosition()->getPositionX() == p_ptrThief->getPosition()->getPositionX())
    {
        if(p_ptrPlayer->getPosition()->getPositionY() == p_ptrThief->getPosition()->getPositionY()){
            cout << "########## \n";
            cout << "Positions equal: Game ends \n";
            cout << "########## \n";
            xRetVal = true;
        } else {
            cout << "########## \n";
            cout << "Positions different: Continue game \n";
            cout << "########## \n";
        }
    }else {
            cout << "########## \n";
            cout << "Positions different: Continue game \n";
            cout << "########## \n";

    }
    if(m_iRoundCounter < 0){
        cout << "######### \n";
        cout << "Rounds over: Thief won! \n";
        cout << "######### \n";
        xRetVal = true;
    }
    return xRetVal;
}

AsyncTask::DoneStatus Game::callPlayGame(GenericAsyncTask* p_ptrTask, void* p_ptrData){
    // preconditions
    if(p_ptrData == NULL)
    {
        nout << "ERROR: AsyncTask::DoneStatus World::call_move(GenericAsyncTask* taskPtr, void* p_ptrData) parameter dataPtr cannot be NULL." << endl;
        return AsyncTask::DS_done;
    }

    Game* ptrGame = static_cast<Game*>(p_ptrData);
    ptrGame->playGame();
    return AsyncTask::DS_cont;
}

void Game::playGame(){
    switch(m_eState){

        case MENU : {
            // show menu
            m_ptrMenuText->reparent_to(m_ptrWindowFramework->get_aspect_2d());

            // start game if Key "s" was pressed
            if(m_vKeymap[K_s_startGame]){
                m_eState = MOVETHIEF;

                // reset key_map for a
                m_vKeymap[K_s_startGame] = false;
                m_vKeymap[K_i_info] = false;
                m_vKeymap[K_c_credits] = false;
                m_ptrMenuText->detach_node();
                m_ptrTextBox->detach_node();
            }

            // show how to play the game
            if(m_vKeymap[K_i_info]){
                m_vKeymap[K_i_info] = false;

                m_ptrTextBox->clear_text();
                m_ptrTextBox->set_text(m_sHowToPlay);
                m_ptrTextBox->reparent_to(m_ptrWindowFramework->get_aspect_2d());
            }

            // show credits
            if(m_vKeymap[K_c_credits]){
                m_vKeymap[K_c_credits] = false;

                m_ptrTextBox->clear_text();
                m_ptrTextBox->set_text(m_sCredits);
                m_ptrTextBox->reparent_to(m_ptrWindowFramework->get_aspect_2d());
            }

            if(m_vKeymap[K_b_back]){
                m_vKeymap[K_b_back] = false;

                m_ptrTextBox->detach_node();
            }

            break;
        }


        case NEWGAME : {
            // reset the positon of the player and thief
            m_ptrPlayer->randomPosition();
            m_ptrThief->randomPosition();
            /// is ok? m_ptrThief->setPosition(1,6);
            int iX = m_ptrPlayer->getPosition()->getPositionX();
            int iY = m_ptrPlayer->getPosition()->getPositionY();
            m_PlayerModel.set_pos((iX*10)+5,(iY*10)+5,0);

            ///TODO: Thief model
            //iX = m_ptrThief->getPosition()->getPositionX();
            //iY = m_ptrThief->getPosition()->getPositionY();
            //m_ThiefModel.set_pos((iX*10)+5,(iY*10)+5,0);

            // reset the positon of the camera
            setCamToWorld();

            // reset the round counter
            m_iRoundCounter = 10;
            m_eState = MENU;

            break;
        }

        case MOVETHIEF : {
            if(m_ptrThief->moveRandom()){
                m_eState = PLAYSCENE;
                //int iX = m_ptrThief->getPosition()->getPositionX();
                //int iY = m_ptrThief->getPosition()->getPositionY();
                //m_ThiefModel.set_pos((iX*10)+5,(iY*10)+5,0);
                SceneNames eSceneName = m_ptrWorld->getSceneName(m_ptrThief->getPosition()->getPositionX(),m_ptrThief->getPosition()->getPositionY());
                m_xGameOver = checkEndOfGame(m_ptrPlayer,m_ptrThief);
                if(m_xGameOver){
                        m_eState = GAMEOVER;
                } else {
                    if(eSceneName == sz_cannon){
                        m_eState = CANNON;
                    }else {
                        m_eState = PLAYSCENE;
                    }
                }
            }
            break;
        }

        case CANNON : {
            if(m_vScenes[sz_cannon]->isRdy()){
                if(!m_vScenes[sz_cannon]->isPlaying()){
                    // set cam to stage
                    setCamToStage();
                    // play cannon scene
                    m_vScenes[sz_cannon]->playScene();


                }
            } else if(m_vScenes[sz_cannon]->isDone()){
                // stop scene when done
                m_vScenes[sz_cannon]->stopScene();
                // set cam to world
                setCamToWorld();
                // move thief to some place on game
                m_ptrThief->randomPosition();
                //int iX = m_ptrThief->getPosition()->getPositionX();
                //int iY = m_ptrThief->getPosition()->getPositionY();
                //m_ThiefModel.set_pos((iX*10)+5,(iY*10)+5,0);
                // play next scene
                m_eState = PLAYSCENE;
            }
            break;
        }


/* #######  BEGIN   PLAYSCENES ########################################################*/
        case PLAYSCENE : {

            // set cam to stage
            setCamToStage();

            // get the name of the scene to be played
            SceneNames eSceneName = m_ptrWorld->getSceneName(m_ptrThief->getPosition()->getPositionX(),m_ptrThief->getPosition()->getPositionY());

                    if(m_vScenes[eSceneName]->isRdy()){
                        if(!m_vScenes[eSceneName]->isPlaying()){
                            m_vScenes[eSceneName]->playScene();
                            nout << "start Scene: " << eSceneName << endl;
                        }
                    } else if(m_vScenes[eSceneName]->isDone()){
                        m_vScenes[eSceneName]->stopScene();
                        m_eState = DICE;

                    }
            break;
        }
/* #######  END   PLAYSCENES ########################################################*/

        case DICE : {
            if(m_ptrDice->throwDice()){

                m_eState = RESET_KEYMAP;
            }
            break;
        }

        case RESET_KEYMAP : {
            m_vKeymap[K_right] = false;
            m_vKeymap[K_left] = false;
            m_vKeymap[K_forward] = false;
            m_vKeymap[K_down] = false;
            m_vKeymap[K_s_startGame] = false;
            m_eState = MOVEPLAYER;
        }

        case MOVEPLAYER : {
            // set cam to world
            setCamToWorld();

            if(m_vKeymap[K_right]){
                int iX = m_ptrPlayer->getPosition()->getPositionX();
                int iY = m_ptrPlayer->getPosition()->getPositionY();
                bool xValid = checkValidMove(m_ptrPlayer,iX+1,iY,m_ptrDice->getValue());
                if(xValid){
                    iX +=1;
                    m_ptrPlayer->setPosition(iX,iY);
                    m_PlayerModel.set_pos((iX*10)+5,(iY*10)+5,0);
                    m_ptrDice->decrement();


                    m_xGameOver = checkEndOfGame(m_ptrPlayer,m_ptrThief);
                    if(m_xGameOver){
                        m_eState = GAMEOVER;
                    } else {
                        if(m_ptrDice->getValue() == 0){
                            m_iRoundCounter--;
                            m_eState = MOVETHIEF;
                        }
                    }
                    m_vKeymap[K_right] = false;
                } else {
                    m_vKeymap[K_right] = false;
                }

            }
            if(m_vKeymap[K_left]){
                int iX = m_ptrPlayer->getPosition()->getPositionX();
                int iY = m_ptrPlayer->getPosition()->getPositionY();
                bool xValid = checkValidMove(m_ptrPlayer,iX-1,iY,m_ptrDice->getValue());
                if(xValid){
                    iX -=1;
                    m_ptrPlayer->setPosition(iX,iY);
                    m_PlayerModel.set_pos((iX*10)+5,(iY*10)+5,0);
                    m_ptrDice->decrement();
                    m_xGameOver = checkEndOfGame(m_ptrPlayer,m_ptrThief);
                    if(m_xGameOver){
                        m_eState = GAMEOVER;
                    } else {
                        if(m_ptrDice->getValue() == 0){
                            m_iRoundCounter--;
                            m_eState = MOVETHIEF;
                        }
                    }
                    m_vKeymap[K_left] = false;
                }else {
                    m_vKeymap[K_left] = false;
                }

            }
            if(m_vKeymap[K_forward]){
                int iX = m_ptrPlayer->getPosition()->getPositionX();
                int iY = m_ptrPlayer->getPosition()->getPositionY();
                bool xValid = checkValidMove(m_ptrPlayer,iX,iY+1,m_ptrDice->getValue());
                if(xValid){
                    iY +=1;
                    m_ptrPlayer->setPosition(iX,iY);
                    m_PlayerModel.set_pos((iX*10)+5,(iY*10)+5,0);
                    m_ptrDice->decrement();
                    m_xGameOver = checkEndOfGame(m_ptrPlayer,m_ptrThief);
                    if(m_xGameOver){
                        m_eState = GAMEOVER;
                    } else {
                        if(m_ptrDice->getValue() == 0){
                            m_iRoundCounter--;
                            m_eState = MOVETHIEF;
                        }

                    m_vKeymap[K_forward] = false;
                    }
                } else {
                    m_vKeymap[K_forward] = false;
                }

            }
            if(m_vKeymap[K_down]){
                int iX = m_ptrPlayer->getPosition()->getPositionX();
                int iY = m_ptrPlayer->getPosition()->getPositionY();
                bool xValid = checkValidMove(m_ptrPlayer,iX,iY-1,m_ptrDice->getValue());
                if(xValid){
                    iY -=1;
                    m_ptrPlayer->setPosition(iX,iY);
                    m_PlayerModel.set_pos((iX*10)+5,(iY*10)+5,0);
                    m_ptrDice->decrement();
                    m_xGameOver = checkEndOfGame(m_ptrPlayer,m_ptrThief);
                    if(m_xGameOver){
                        m_eState = GAMEOVER;
                    } else {
                        if(m_ptrDice->getValue() == 0){
                            m_iRoundCounter--;
                            m_eState = MOVETHIEF;
                        }
                    }
                    m_vKeymap[K_down] = false;
                } else {
                    m_vKeymap[K_down] = false;
                }
            }
            break;
        }

        case GAMEOVER : {
            nout << "GAME OVER! Press Esc";
            m_eState = NEWGAME;
            break;
        }

    }
}


void Game::setCamToWorld(){
    m_Camera.set_pos(50, -40, 140);
    m_Camera.set_hpr(0,-60, 0);
}

void Game::setCamToStage(){
    m_Camera.set_pos(50, 80, 30);
    m_Camera.set_hpr(0,-10, 0);
}

template<int iKey,bool xValue>
void Game::callSetKey(const Event* p_ptrEvent,void* p_ptrData){
    // preconditions
    if(p_ptrData == NULL)
    {
        nout << "ERROR: template<int key> void World::call_set_key(const Event* eventPtr, void* dataPtr) "
                "parameter dataPtr cannot be NULL." << endl;
        return;
    }
    if(iKey < 0 || iKey >= K_EndKey)
    {
        nout << "ERROR: template<int key> void World::call_set_key(const Event* eventPtr, void* dataPtr) "
                "parameter key is out of range: " << iKey << endl;
        return;
    }

    static_cast<Game*>(p_ptrData)->setKey(static_cast<Key>(iKey), xValue);
}

void Game::setKey(Key p_eKey,bool p_xValue){
    bool xSwitch = true;
    if(p_eKey < 0 || (unsigned int)p_eKey >= m_vKeymap.size())
    {
        nout << "void Game::setKey(Key p_eKey,bool p_xValue) parameter key is out of range: " << p_eKey << endl;
    }

    for(int i=0; i < K_EndKey; i++)
    {
        if(m_vKeymap[i] == true)
        xSwitch = false;
    }

    if(xSwitch == true)
        m_vKeymap[p_eKey] = p_xValue;
    return;
}

void Game::sys_exit(const Event* eventPtr, void* dataPtr){
    exit(0);
}

