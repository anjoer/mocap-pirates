#include "Scene_swordfight.h"

Scene_swordfight::Scene_swordfight(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_swordfight::~Scene_swordfight(){
    //dtor
}

bool Scene_swordfight::playScene(){
        m_xIsRdy = false;
        CActor kaemper = (*m_ptrActors)["Swordfight"];
        (*m_ptrAssets)["Schwert01"].reparent_to(kaemper.expose_joint("schwertkampf:Character1_RightHand"));
        (*m_ptrAssets)["Schwert02"].reparent_to(kaemper.expose_joint("schwertkampf:Jack_V2:Character1_RightHand"));
        (*m_ptrAssets)["Schwert01"].set_scale(0.03);
        (*m_ptrAssets)["Schwert02"].set_scale(0.03);
        (*m_ptrAssets)["Schwert02"].set_h(-60);
        (*m_ptrAssets)["Schwert02"].set_scale(0.03);

        (*m_ptrAssets)["BuehneStadt"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Swordfight"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Swordfight"].set_pos((*m_ptrAssets)["BuehneStadt"].get_pos());
        (*m_ptrActors)["Swordfight"].set_scale(15.0);
        (*m_ptrActors)["Swordfight"].set_h(-90);
        (*m_ptrActors)["Swordfight"].play("Swordfight");

    return true;
}

bool Scene_swordfight::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Swordfight"].is_playing();
    return m_xIsPlaying;
}

bool Scene_swordfight::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Swordfight"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_swordfight::stopScene(){
    (*m_ptrActors)["Swordfight"].stop("Swordfight");
    (*m_ptrActors)["Swordfight"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_swordfight::isRdy(){

    return m_xIsRdy;
}
