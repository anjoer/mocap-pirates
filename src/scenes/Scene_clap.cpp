#include "Scene_clap.h"

Scene_clap::Scene_clap(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp)
{
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_clap::~Scene_clap(){
    //dtor
}

bool Scene_clap::playScene(){
        m_xIsRdy = false;
        (*m_ptrActors)["Clap"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Clap"].set_pos((*m_ptrAssets)["BuehneStadt"].get_pos());
        (*m_ptrActors)["Clap"].set_scale(10.0);
        (*m_ptrActors)["Clap"].play("Clap");
        // no accessories for this scene

    return true;
}

bool Scene_clap::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Clap"].is_playing();
    return m_xIsPlaying;
}

bool Scene_clap::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Clap"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_clap::stopScene(){
    (*m_ptrActors)["Clap"].stop("Clap");

    (*m_ptrActors)["Clap"].detach_node();
    (*m_ptrAssets)["BuehneStadt"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_clap::isRdy(){

    return m_xIsRdy;
}
