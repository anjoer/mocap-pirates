#include "Scene_surfing.h"

Scene_surfing::Scene_surfing(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_surfing::~Scene_surfing(){
    //dtor
}

bool Scene_surfing::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));






        (*m_ptrActors)["Surfing"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Surfing"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Surfing"].set_y((*m_ptrActors)["Surfing"].get_y()+60);
        (*m_ptrActors)["Surfing"].set_scale(10.0);
        (*m_ptrActors)["Surfing"].set_h(60);
        (*m_ptrActors)["Surfing"].play("Surfing");

    return true;
}

bool Scene_surfing::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Surfing"].is_playing();
    return m_xIsPlaying;
}

bool Scene_surfing::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Surfing"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_surfing::stopScene(){
    (*m_ptrActors)["Surfing"].stop("Surfing");
    (*m_ptrActors)["Surfing"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_surfing::isRdy(){

    return m_xIsRdy;
}
