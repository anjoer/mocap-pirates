#include "Scene_kapitaen.h"

Scene_kapitaen::Scene_kapitaen(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_kapitaen::~Scene_kapitaen(){
    //dtor
}

bool Scene_kapitaen::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneSchiff"].reparent_to((*m_renderNp));

        (*m_ptrAssets)["BuehneSchiff"].set_pos(60, 160, 0);

        (*m_ptrActors)["Kapitaen"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Kapitaen"].set_pos((*m_ptrAssets)["BuehneSchiff"].get_pos());
        (*m_ptrActors)["Kapitaen"].set_x((*m_ptrActors)["Kapitaen"].get_x()-12);
        (*m_ptrActors)["Kapitaen"].set_z((*m_ptrActors)["Kapitaen"].get_z()+2);
        (*m_ptrActors)["Kapitaen"].set_scale(10.0);
        (*m_ptrActors)["Kapitaen"].set_h(60);
        (*m_ptrActors)["Kapitaen"].play("Kapitaen");

    return true;
}

bool Scene_kapitaen::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Kapitaen"].is_playing();
    return m_xIsPlaying;
}

bool Scene_kapitaen::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Kapitaen"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_kapitaen::stopScene(){
    (*m_ptrActors)["Kapitaen"].stop("Kapitaen");
    (*m_ptrActors)["Kapitaen"].detach_node();
    (*m_ptrAssets)["BuehneSchiff"].detach_node();
    (*m_ptrAssets)["BuehneSchiff"].set_pos(50, 160, 0);
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_kapitaen::isRdy(){

    return m_xIsRdy;
}
