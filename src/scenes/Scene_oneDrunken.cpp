#include "Scene_oneDrunken.h"

Scene_oneDrunken::Scene_oneDrunken(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}


Scene_oneDrunken::~Scene_oneDrunken(){
    //dtor
}

bool Scene_oneDrunken::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));

        CActor drunkp = (*m_ptrActors)["OneDrunken"];
        (*m_ptrAssets)["Krug"].set_scale(.5);
        (*m_ptrAssets)["Krug"].set_p(30);
        (*m_ptrAssets)["Krug"].set_x((*m_ptrAssets)["Krug"].get_x()-0.19);
        (*m_ptrAssets)["Krug"].set_z(-0.03);
        (*m_ptrAssets)["Krug"].reparent_to(drunkp.expose_joint("Betrunkener_300:Character1_RightHand"));

        (*m_ptrActors)["OneDrunken"].reparent_to((*m_renderNp));
        (*m_ptrActors)["OneDrunken"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["OneDrunken"].set_scale(15.0);
        (*m_ptrActors)["OneDrunken"].set_h(-30);
        (*m_ptrActors)["OneDrunken"].set_z((*m_ptrActors)["OneDrunken"].get_z()+2);

        (*m_ptrActors)["OneDrunken"].play("OneDrunken");

    return true;
}

bool Scene_oneDrunken::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["OneDrunken"].is_playing();
    return m_xIsPlaying;
}

bool Scene_oneDrunken::isDone(){
    m_xIsPlaying = (*m_ptrActors)["OneDrunken"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_oneDrunken::stopScene(){
    (*m_ptrActors)["OneDrunken"].stop("OneDrunken");
    (*m_ptrActors)["OneDrunken"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();
    (*m_ptrAssets)["Krug"].detach_node();
    (*m_ptrAssets)["Krug"].set_x((*m_ptrAssets)["Krug"].get_x()+0.19);
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_oneDrunken::isRdy(){

    return m_xIsRdy;
}
