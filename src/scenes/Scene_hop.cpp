#include "Scene_hop.h"

Scene_hop::Scene_hop(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_hop::~Scene_hop(){
    //dtor
}

bool Scene_hop::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStadt"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Hop"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Hop"].set_pos((*m_ptrAssets)["BuehneStadt"].get_pos());
        (*m_ptrActors)["Hop"].set_scale(15.0);
        (*m_ptrActors)["Hop"].set_h(30);
        (*m_ptrActors)["Hop"].set_z((*m_ptrActors)["Hop"].get_z()+3);
        (*m_ptrActors)["Hop"].play("Hop");

    return true;
}

bool Scene_hop::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Hop"].is_playing();
    return m_xIsPlaying;
}

bool Scene_hop::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Hop"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_hop::stopScene(){
    (*m_ptrActors)["Hop"].stop("Hop");
    (*m_ptrActors)["Hop"].detach_node();
    (*m_ptrAssets)["BuehneStadt"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_hop::isRdy(){

    return m_xIsRdy;
}
