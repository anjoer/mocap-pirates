#include "Scene_mastclimb.h"

Scene_mastclimb::Scene_mastclimb(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp)
{
   m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_mastclimb::~Scene_mastclimb(){
    //dtor
}

bool Scene_mastclimb::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneSchiff"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Mastclimb"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Mastclimb"].set_pos((*m_ptrAssets)["BuehneSchiff"].get_pos());
        /// TODO: auf Buehne
        (*m_ptrActors)["Mastclimb"].set_scale(15.0);
        (*m_ptrActors)["Mastclimb"].set_h(90);
        (*m_ptrActors)["Mastclimb"].set_x((*m_ptrActors)["Mastclimb"].get_x()+14);
        (*m_ptrActors)["Mastclimb"].play("Mastclimb");

    return true;
}

bool Scene_mastclimb::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Mastclimb"].is_playing();
    return m_xIsPlaying;
}

bool Scene_mastclimb::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Mastclimb"].is_playing();
    (*m_ptrActors)["Mastclimb"].set_z((*m_ptrActors)["Mastclimb"].get_z()+0.04);
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_mastclimb::stopScene(){
    (*m_ptrActors)["Mastclimb"].stop("Mastclimb");
    (*m_ptrActors)["Mastclimb"].detach_node();
    (*m_ptrAssets)["BuehneSchiff"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_mastclimb::isRdy(){

    return m_xIsRdy;
}
