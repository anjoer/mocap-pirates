#include "Scene_cannon.h"

Scene_cannon::Scene_cannon(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_cannon::~Scene_cannon(){
    //dtor
}

bool Scene_cannon::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));

        (*m_ptrActors)["Cannon"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Cannon"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Cannon"].set_x((*m_ptrActors)["Cannon"].get_x()-7);
        (*m_ptrActors)["Cannon"].set_y((*m_ptrActors)["Cannon"].get_y()+7);
        (*m_ptrActors)["Cannon"].set_h(120);
        (*m_ptrActors)["Cannon"].set_scale(7);
        (*m_ptrActors)["Cannon"].play("Cannon");

    return true;
}

bool Scene_cannon::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Cannon"].is_playing();
    return m_xIsPlaying;
}

bool Scene_cannon::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Cannon"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_cannon::stopScene(){
    (*m_ptrActors)["Cannon"].stop("Cannon");

    (*m_ptrActors)["Cannon"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();

    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_cannon::isRdy(){

    return m_xIsRdy;
}
