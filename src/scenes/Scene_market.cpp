#include "Scene_market.h"


Scene_market::Scene_market(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp)
{
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_market::~Scene_market()
{
    //dtor
}

bool Scene_market::playScene(){
        m_xIsRdy = false;
        /// todo set andere stage
        (*m_ptrAssets)["BuehneStadt"].reparent_to((*m_renderNp));
        // Add marktstand
        (*m_ptrAssets)["Marktstand"].set_pos((*m_ptrAssets)["BuehneStadt"].get_pos());
        (*m_ptrAssets)["Marktstand"].set_scale(12.0);
        (*m_ptrAssets)["Marktstand"].set_x((*m_ptrAssets)["Marktstand"].get_x()+10);
        (*m_ptrAssets)["Marktstand"].set_y((*m_ptrAssets)["Marktstand"].get_y()+15);
        (*m_ptrAssets)["Marktstand"].set_h(160);
        (*m_ptrAssets)["Marktstand"].reparent_to((*m_renderNp));
        // scene
        (*m_ptrActors)["Market"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Market"].set_pos((*m_ptrAssets)["BuehneStadt"].get_pos());
        (*m_ptrActors)["Market"].set_h(50);
        (*m_ptrActors)["Market"].set_scale(15.0);
        (*m_ptrActors)["Market"].play("market");


    return true;
}

bool Scene_market::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Market"].is_playing();
    return m_xIsPlaying;
}

bool Scene_market::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Market"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_market::stopScene(){
    (*m_ptrActors)["Market"].stop("market");
    (*m_ptrActors)["Market"].detach_node();
    (*m_ptrAssets)["Marktstand"].detach_node();
    (*m_ptrAssets)["BuehneStadt"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_market::isRdy(){

    return m_xIsRdy;
}
