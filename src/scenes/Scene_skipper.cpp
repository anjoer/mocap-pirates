#include "Scene_skipper.h"

Scene_skipper::Scene_skipper(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_skipper::~Scene_skipper(){
    //dtor
}

bool Scene_skipper::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneSchiff"].reparent_to((*m_renderNp));
        //
        CActor steuermann = (*m_ptrActors)["Skipper"];
        (*m_ptrAssets)["Steuerrad"].reparent_to(steuermann.expose_joint("Steuermann_260:Character1_LeftHand"));
        (*m_ptrAssets)["Steuerrad"].set_p(180);

        (*m_ptrActors)["Skipper"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Skipper"].set_pos((*m_ptrAssets)["BuehneSchiff"].get_pos());
        /// TODO: auf Buehne schiff
        (*m_ptrActors)["Skipper"].set_scale(10.0);
        (*m_ptrActors)["Skipper"].play("Skipper");

    return true;
}

bool Scene_skipper::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Skipper"].is_playing();
    return m_xIsPlaying;
}

bool Scene_skipper::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Skipper"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_skipper::stopScene(){
    (*m_ptrActors)["Skipper"].stop("Skipper");
    (*m_ptrActors)["Skipper"].detach_node();
    (*m_ptrAssets)["BuehneSchiff"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_skipper::isRdy(){

    return m_xIsRdy;
}
