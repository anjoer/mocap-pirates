#include "Scene_shovel.h"

Scene_shovel::Scene_shovel(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_shovel::~Scene_shovel(){
    //dtor
}

bool Scene_shovel::playScene(){
        m_xIsRdy = false;
        CActor artist = (*m_ptrActors)["Shovel"];
        (*m_ptrAssets)["Schaufel"].reparent_to(artist.expose_joint("schaufeln:Character1_LeftHand"));
        (*m_ptrAssets)["Schaufel"].set_scale(0.8);
        (*m_ptrAssets)["Schaufel"].set_x(0.04);
        (*m_ptrAssets)["Schaufel"].set_p(90);

        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Shovel"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Shovel"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        /// TODO: Schaufel
        (*m_ptrActors)["Shovel"].set_scale(10.0);



        (*m_ptrActors)["Shovel"].play("Shovel");

    return true;
}

bool Scene_shovel::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Shovel"].is_playing();
    return m_xIsPlaying;
}

bool Scene_shovel::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Shovel"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_shovel::stopScene(){
    (*m_ptrActors)["Shovel"].stop("Shovel");
    (*m_ptrAssets)["Schaufel"].set_z((*m_ptrAssets)["Schaufel"].get_z()+0.5);
    (*m_ptrActors)["Shovel"].detach_node();
    (*m_ptrAssets)["Schaufel"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_shovel::isRdy(){

    return m_xIsRdy;
}
