#include "Scene_sandburg.h"

Scene_sandburg::Scene_sandburg(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_sandburg::~Scene_sandburg(){
    //dtor
}

bool Scene_sandburg::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Sandburg"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Sandburg"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Sandburg"].set_scale(15.0);
        (*m_ptrActors)["Sandburg"].play("Sandburg");

    return true;
}

bool Scene_sandburg::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Sandburg"].is_playing();
    return m_xIsPlaying;
}

bool Scene_sandburg::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Sandburg"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_sandburg::stopScene(){
    (*m_ptrActors)["Sandburg"].stop("Sandburg");
    (*m_ptrActors)["Sandburg"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_sandburg::isRdy(){

    return m_xIsRdy;
}
