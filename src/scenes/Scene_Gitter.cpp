#include "Scene_Gitter.h"

Scene_Gitter::Scene_Gitter(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_Gitter::~Scene_Gitter(){
    //dtor
}

bool Scene_Gitter::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));

        (*m_ptrAssets)["Gitter"].reparent_to((*m_renderNp));
        (*m_ptrAssets)["Gitter"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrAssets)["Gitter"].set_h(70);
        (*m_ptrAssets)["Gitter"].set_x((*m_ptrAssets)["Gitter"].get_x()-8.5);
        (*m_ptrAssets)["Gitter"].set_scale(2);

        (*m_ptrActors)["Gitter"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Gitter"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Gitter"].set_scale(15.0);
        (*m_ptrActors)["Gitter"].set_z((*m_ptrActors)["Gitter"].get_z()+1.5);
        (*m_ptrActors)["Gitter"].set_h(70);
        (*m_ptrActors)["Gitter"].play("Gitter");

    return true;
}

bool Scene_Gitter::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Gitter"].is_playing();
    return m_xIsPlaying;
}

bool Scene_Gitter::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Gitter"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_Gitter::stopScene(){
    (*m_ptrActors)["Gitter"].stop("Gitter");
    (*m_ptrActors)["Gitter"].detach_node();
    (*m_ptrAssets)["Gitter"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_Gitter::isRdy(){

    return m_xIsRdy;
}
