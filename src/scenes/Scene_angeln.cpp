#include "Scene_angeln.h"


Scene_angeln::Scene_angeln(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_angeln::~Scene_angeln(){
    //dtor
}

bool Scene_angeln::playScene(){
        m_xIsRdy = false;
        // load BuehneStrand
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));

        // get a joint of the Angler and attach the Angel to the joint
        CActor angler = (*m_ptrActors)["Angeln"];
        (*m_ptrAssets)["Angel"].reparent_to(angler.expose_joint("Angeln:Character1_RightHand"));
        (*m_ptrAssets)["Angel"].set_scale(1);
        (*m_ptrAssets)["Angel"].set_r(-90);

        // set the Stamm in scene
        (*m_ptrAssets)["Stamm"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrAssets)["Stamm"].set_scale(0.7);
        (*m_ptrAssets)["Stamm"].set_y((*m_ptrAssets)["Stamm"].get_y()+2);
        (*m_ptrAssets)["Stamm"].set_x((*m_ptrAssets)["Stamm"].get_x()-3);
        (*m_ptrAssets)["Stamm"].reparent_to((*m_renderNp));

        // set topf
        (*m_ptrAssets)["Topf"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrAssets)["Topf"].set_scale(7);
        (*m_ptrAssets)["Topf"].set_y((*m_ptrAssets)["Topf"].get_y()-7);
        (*m_ptrAssets)["Topf"].set_x((*m_ptrAssets)["Topf"].get_x()-3);
        (*m_ptrAssets)["Topf"].reparent_to((*m_renderNp));

        // set the scene in position
        (*m_ptrActors)["Angeln"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Angeln"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Angeln"].set_z((*m_ptrActors)["Angeln"].get_z()+1);
        (*m_ptrActors)["Angeln"].set_scale(15.0);
        (*m_ptrActors)["Angeln"].set_h(-50);
        (*m_ptrActors)["Angeln"].play("Angeln");

    return true;
}

bool Scene_angeln::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Angeln"].is_playing();
    return m_xIsPlaying;
}

bool Scene_angeln::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Angeln"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_angeln::stopScene(){
    (*m_ptrActors)["Angeln"].stop("Angeln");

    (*m_ptrActors)["Angeln"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();
    (*m_ptrAssets)["Stamm"].detach_node();
    (*m_ptrAssets)["Topf"].detach_node();

    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_angeln::isRdy(){

    return m_xIsRdy;
}
