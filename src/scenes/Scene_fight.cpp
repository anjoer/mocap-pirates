#include "Scene_fight.h"
/// todo delete include, this was only for playing a scene with a different frame rate
//#include "animControlCollection.h"
//#include "auto_bind.h"

Scene_fight::Scene_fight(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp)
{
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_fight::~Scene_fight()
{
    //dtor
}

bool Scene_fight::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));

        (*m_ptrActors)["Fight"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Fight"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Fight"].set_z((*m_ptrActors)["Fight"].get_z()+1.5);
        (*m_ptrActors)["Fight"].set_scale(15.0);
        (*m_ptrActors)["Fight"].set_h(+50);
        (*m_ptrActors)["Fight"].play("fight");
    return true;
}

bool Scene_fight::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Fight"].is_playing();
    return m_xIsPlaying;
}

bool Scene_fight::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Fight"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_fight::stopScene(){
    (*m_ptrActors)["Fight"].stop("fight");

    (*m_ptrActors)["Fight"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();

    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_fight::isRdy(){

    return m_xIsRdy;
}
