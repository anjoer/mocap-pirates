#include "Scene_prostitute.h"

Scene_prostitute::Scene_prostitute(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_prostitute::~Scene_prostitute(){
    //dtor
}

bool Scene_prostitute::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStadt"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Prostitute"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Prostitute"].set_pos((*m_ptrAssets)["BuehneStadt"].get_pos());
        (*m_ptrActors)["Prostitute"].set_scale(15.0);
        (*m_ptrActors)["Prostitute"].set_z((*m_ptrActors)["Prostitute"].get_z()+2);
        (*m_ptrActors)["Prostitute"].play("Prostitute");

    return true;
}

bool Scene_prostitute::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Prostitute"].is_playing();
    return m_xIsPlaying;
}

bool Scene_prostitute::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Prostitute"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_prostitute::stopScene(){
    (*m_ptrActors)["Prostitute"].stop("Prostitute");
    (*m_ptrActors)["Prostitute"].detach_node();
    (*m_ptrAssets)["BuehneStadt"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_prostitute::isRdy(){

    return m_xIsRdy;
}
