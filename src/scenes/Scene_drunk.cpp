#include "Scene_drunk.h"

Scene_drunk::Scene_drunk(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_drunk::~Scene_drunk()
{
    //dtor
}
bool Scene_drunk::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStadt"].reparent_to((*m_renderNp));

        (*m_ptrActors)["Drunk"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Drunk"].set_pos((*m_ptrAssets)["BuehneStadt"].get_pos());
        (*m_ptrActors)["Drunk"].set_z((*m_ptrActors)["Drunk"].get_z()+1);
        (*m_ptrActors)["Drunk"].set_scale(15.0);
        (*m_ptrActors)["Drunk"].set_h(140);
        (*m_ptrActors)["Drunk"].play("Drunk");

    return true;
}

bool Scene_drunk::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Drunk"].is_playing();
    return m_xIsPlaying;
}

bool Scene_drunk::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Drunk"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_drunk::stopScene(){
    (*m_ptrActors)["Drunk"].stop("Drunk");

    (*m_ptrActors)["Drunk"].detach_node();
    (*m_ptrAssets)["BuehneStadt"].detach_node();

    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_drunk::isRdy(){

    return m_xIsRdy;
}
