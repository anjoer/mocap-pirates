#include "Scene_lookout.h"

Scene_lookout::Scene_lookout(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_lookout::~Scene_lookout(){
    //dtor
}

bool Scene_lookout::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));

        (*m_ptrAssets)["Plattform"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrAssets)["Plattform"].set_scale(7.0);
        (*m_ptrAssets)["Plattform"].set_z(15);
        (*m_ptrAssets)["Plattform"].reparent_to((*m_renderNp));

        (*m_ptrActors)["Lookout"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Lookout"].set_pos((*m_ptrAssets)["Plattform"].get_pos());
        (*m_ptrActors)["Lookout"].set_z(15);
        (*m_ptrActors)["Lookout"].set_h(30);
        (*m_ptrActors)["Lookout"].set_scale(10.0);
        (*m_ptrActors)["Lookout"].play("Lookout");

    return true;
}

bool Scene_lookout::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Lookout"].is_playing();
    return m_xIsPlaying;
}

bool Scene_lookout::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Lookout"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_lookout::stopScene(){
    (*m_ptrActors)["Lookout"].stop("Lookout");
    (*m_ptrActors)["Lookout"].detach_node();
    (*m_ptrAssets)["Plattform"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_lookout::isRdy(){

    return m_xIsRdy;
}
