#include "Scene_marschieren.h"

Scene_marschieren::Scene_marschieren(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_marschieren::~Scene_marschieren(){
    //dtor
}

bool Scene_marschieren::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Marschieren"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Marschieren"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Marschieren"].set_scale(15.0);
        (*m_ptrActors)["Marschieren"].set_z((*m_ptrActors)["Marschieren"].get_z()+2);
        (*m_ptrActors)["Marschieren"].set_x((*m_ptrActors)["Marschieren"].get_x()-8);
        (*m_ptrActors)["Marschieren"].set_h(90);
        (*m_ptrActors)["Marschieren"].play("Marschieren");
    return true;
}

bool Scene_marschieren::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Marschieren"].is_playing();
    return m_xIsPlaying;
}

bool Scene_marschieren::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Marschieren"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_marschieren::stopScene(){
    (*m_ptrActors)["Marschieren"].stop("Marschieren");
    (*m_ptrActors)["Marschieren"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_marschieren::isRdy(){

    return m_xIsRdy;
}
