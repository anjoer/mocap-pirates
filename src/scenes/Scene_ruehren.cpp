#include "Scene_ruehren.h"

Scene_ruehren::Scene_ruehren(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp)
{
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_ruehren::~Scene_ruehren()
{
    //dtor
}
bool Scene_ruehren::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));
        // load Topf
        (*m_ptrAssets)["Topf"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrAssets)["Topf"].set_scale(22.0);
        (*m_ptrAssets)["Topf"].set_x((*m_ptrAssets)["Topf"].get_x()-3);
        (*m_ptrAssets)["Topf"].set_y((*m_ptrAssets)["Topf"].get_y()-8);
        (*m_ptrAssets)["Topf"].set_h(160);
        (*m_ptrAssets)["Topf"].reparent_to((*m_renderNp));

        // load loeffel and attach
        CActor koch = (*m_ptrActors)["Ruehren"];
        (*m_ptrAssets)["Loeffel"].reparent_to(koch.expose_joint("Character1_LeftHand"));
        (*m_ptrAssets)["Loeffel"].set_pos(0.1,0,-0.1);
        (*m_ptrAssets)["Loeffel"].set_p(-90);
        (*m_ptrAssets)["Loeffel"].set_scale(1.8);

        // load actor
        (*m_ptrActors)["Ruehren"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Ruehren"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Ruehren"].set_y((*m_ptrActors)["Ruehren"].get_y()+5);
        (*m_ptrActors)["Ruehren"].set_x((*m_ptrActors)["Ruehren"].get_x()+5);
        (*m_ptrActors)["Ruehren"].set_scale(10.0);
        (*m_ptrActors)["Ruehren"].play("Ruehren");

    return true;
}

bool Scene_ruehren::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Ruehren"].is_playing();
    return m_xIsPlaying;
}

bool Scene_ruehren::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Ruehren"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_ruehren::stopScene(){
    (*m_ptrActors)["Ruehren"].stop("Ruehren");
    (*m_ptrActors)["Ruehren"].detach_node();
    (*m_ptrAssets)["Topf"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_ruehren::isRdy(){

    return m_xIsRdy;
}
