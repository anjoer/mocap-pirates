#include "Scene_dance.h"

Scene_dance::Scene_dance(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_dance::~Scene_dance(){
    //dtor
}

bool Scene_dance::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStadt"].reparent_to((*m_renderNp));

        (*m_ptrActors)["Dance"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Dance"].set_pos((*m_ptrAssets)["BuehneStadt"].get_pos());
        (*m_ptrActors)["Dance"].set_scale(15.0);
        (*m_ptrActors)["Dance"].play("Dance");

    return true;
}

bool Scene_dance::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Dance"].is_playing();
    return m_xIsPlaying;
}

bool Scene_dance::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Dance"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_dance::stopScene(){
    (*m_ptrActors)["Dance"].stop("Dance");

    (*m_ptrActors)["Dance"].detach_node();
    (*m_ptrAssets)["BuehneStadt"].detach_node();

    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_dance::isRdy(){

    return m_xIsRdy;
}
