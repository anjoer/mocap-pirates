#include "Scene_saw.h"

Scene_saw::Scene_saw(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_saw::~Scene_saw(){
    //dtor
}

bool Scene_saw::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));
        // attach saw
        CActor man = (*m_ptrActors)["Saw"];
        (*m_ptrAssets)["Saege"].reparent_to(man.expose_joint("saegen2:John_v3:Character1_LeftHand"));
        (*m_ptrAssets)["Saege"].set_pos(0.1,0,-0.1);
        (*m_ptrAssets)["Saege"].set_h(-60);
        (*m_ptrAssets)["Saege"].set_r(90);
        (*m_ptrAssets)["Saege"].set_scale(1.3);

        (*m_ptrAssets)["Stamm"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrAssets)["Stamm"].set_r(90);
        (*m_ptrAssets)["Stamm"].set_h(90);
        (*m_ptrAssets)["Stamm"].set_scale(0.9);
        (*m_ptrAssets)["Stamm"].set_z((*m_ptrAssets)["Stamm"].get_z()+5);
        (*m_ptrAssets)["Stamm"].set_y((*m_ptrAssets)["Stamm"].get_y()-8);
        (*m_ptrAssets)["Stamm"].reparent_to((*m_renderNp));

        (*m_ptrAssets)["Stamm1"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrAssets)["Stamm1"].set_r(90);
        (*m_ptrAssets)["Stamm1"].set_h(120);
        (*m_ptrAssets)["Stamm1"].set_scale(0.7);
        (*m_ptrAssets)["Stamm1"].set_z((*m_ptrAssets)["Stamm"].get_z()+3);
        (*m_ptrAssets)["Stamm1"].set_y((*m_ptrAssets)["Stamm"].get_y()+17);
        (*m_ptrAssets)["Stamm1"].reparent_to((*m_renderNp));

        (*m_ptrActors)["Saw"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Saw"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Saw"].set_x((*m_ptrActors)["Saw"].get_x()+10);
        (*m_ptrActors)["Saw"].set_scale(15.0);
        (*m_ptrActors)["Saw"].play("Saw");

    return true;
}

bool Scene_saw::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Saw"].is_playing();
    return m_xIsPlaying;
}

bool Scene_saw::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Saw"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_saw::stopScene(){
    (*m_ptrActors)["Saw"].stop("Saw");
    (*m_ptrActors)["Saw"].detach_node();
    (*m_ptrAssets)["Stamm"].detach_node();
    (*m_ptrAssets)["Stamm1"].detach_node();
    (*m_ptrAssets)["Saege"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_saw::isRdy(){

    return m_xIsRdy;
}
