#include "Scene_handstand.h"

Scene_handstand::Scene_handstand(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_handstand::~Scene_handstand(){
    //dtor
}

bool Scene_handstand::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));

        (*m_ptrActors)["Handstand"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Handstand"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Handstand"].set_scale(15.0);
        (*m_ptrActors)["Handstand"].set_h(80);
        (*m_ptrActors)["Handstand"].play("Handstand");

    return true;
}

bool Scene_handstand::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Handstand"].is_playing();
    return m_xIsPlaying;
}

bool Scene_handstand::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Handstand"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_handstand::stopScene(){
    (*m_ptrActors)["Handstand"].stop("Handstand");
    (*m_ptrActors)["Handstand"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_handstand::isRdy(){

    return m_xIsRdy;
}
