#include "Scene_geist.h"
#include "cActor.h"

Scene_geist::Scene_geist(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_geist::~Scene_geist(){
    //dtor
}

bool Scene_geist::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStrand"].reparent_to((*m_renderNp));
        // add head to ghost
        CActor geist = (*m_ptrActors)["Geist"];
        (*m_ptrAssets)["Kopf"].reparent_to(geist.expose_joint("Character1_LeftHand"));
        (*m_ptrAssets)["Kopf"].set_pos(0.1,0,-0.1);
        (*m_ptrAssets)["Kopf"].set_p(200);
        (*m_ptrAssets)["Kopf"].set_h(200);
        (*m_ptrActors)["Geist"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Geist"].set_pos((*m_ptrAssets)["BuehneStrand"].get_pos());
        (*m_ptrActors)["Geist"].set_scale(15.0);
        (*m_ptrActors)["Geist"].set_h(+50);
        (*m_ptrActors)["Geist"].play("Geist");

    return true;
}

bool Scene_geist::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Geist"].is_playing();
    return m_xIsPlaying;
}

bool Scene_geist::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Geist"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_geist::stopScene(){
    (*m_ptrActors)["Geist"].stop("Geist");

    (*m_ptrActors)["Geist"].detach_node();
    (*m_ptrAssets)["BuehneStrand"].detach_node();

    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_geist::isRdy(){

    return m_xIsRdy;
}
