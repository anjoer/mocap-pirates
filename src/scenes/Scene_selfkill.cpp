#include "Scene_selfkill.h"

Scene_selfkill::Scene_selfkill(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp){
    m_ptrActors = p_ptrActors;
    m_ptrAssets = p_ptrAssets;
    m_renderNp = p_ptrRenderNp;
    m_xIsPlaying = false;
    m_xIsDone = false;
    m_xIsRdy = true;
}

Scene_selfkill::~Scene_selfkill(){
    //dtor
}

bool Scene_selfkill::playScene(){
        m_xIsRdy = false;
        (*m_ptrAssets)["BuehneStadt"].reparent_to((*m_renderNp));

        CActor mord = (*m_ptrActors)["Selfkill"];
        (*m_ptrAssets)["Pistole"].reparent_to(mord.expose_joint("Selbstmord:Character1_RightHand"));
        (*m_ptrAssets)["Pistole"].set_scale(1);
        (*m_ptrAssets)["Pistole"].set_r(-90);
        (*m_ptrAssets)["Pistole"].set_x((*m_ptrAssets)["Pistole"].get_x()-0.08);

        (*m_ptrActors)["Selfkill"].reparent_to((*m_renderNp));
        (*m_ptrActors)["Selfkill"].set_pos((*m_ptrAssets)["BuehneStadt"].get_pos());
        (*m_ptrActors)["Selfkill"].set_scale(15.0);
        (*m_ptrActors)["Selfkill"].set_z((*m_ptrActors)["Selfkill"].get_z()+2);
        (*m_ptrActors)["Selfkill"].play("Selfkill");

    return true;
}

bool Scene_selfkill::isPlaying(){
    m_xIsPlaying = (*m_ptrActors)["Selfkill"].is_playing();
    return m_xIsPlaying;
}

bool Scene_selfkill::isDone(){
    m_xIsPlaying = (*m_ptrActors)["Selfkill"].is_playing();
    m_xIsDone = !m_xIsPlaying;
    return m_xIsDone;
}


bool Scene_selfkill::stopScene(){
    (*m_ptrActors)["Selfkill"].stop("Selfkill");
    (*m_ptrActors)["Selfkill"].detach_node();
    (*m_ptrAssets)["BuehneStadt"].detach_node();
    m_xIsDone = false;
    m_xIsPlaying = false;
    m_xIsRdy = true;
    return true;
}

bool Scene_selfkill::isRdy(){

    return m_xIsRdy;
}
