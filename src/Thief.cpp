#include "Thief.h"


Thief::Thief()
{
    //Init Step
    this->setStepCounter(0);
    //init Thief random position
    this->randomPosition();
    //ctor
}

Thief::~Thief()
{
    //dtor
}

bool Thief::moveRandom(){
    bool xRetVal = false;
    int iX = getPosition()->getPositionX();
    int iY = getPosition()->getPositionY();
    int iDest =rand() % 12 +1;

    if(stepcounter < 1){
        switch(iDest){
            case 1: {
                if(dLock != D_left){
                    this->direction = D_left;
                    dLock = D_right;
                    stepcounter = 1;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 2: {
                if(dLock != D_right){
                    direction = D_right;
                    dLock = D_left;
                    stepcounter = 1;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 3: {
                if(dLock != D_top){
                    direction = D_top;
                    dLock = D_down;
                    stepcounter = 1;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 4: {
                if(dLock != D_down){
                    direction = D_down;
                    dLock = D_top;
                    stepcounter = 1;
                }else {
                    dLock = D_random;
                }
                break;
            }

            case 5: {
                if(dLock != D_left){
                    direction = D_left;
                    dLock = D_right;
                    stepcounter = 2;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 6: {
                if(dLock != D_right){
                    direction = D_right;
                    dLock = D_left;
                    stepcounter = 2;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 7: {
                if(dLock != D_top){
                    direction = D_top;
                    dLock = D_down;
                    stepcounter = 2;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 8: {
                if(dLock != D_down){
                    direction = D_down;
                    dLock = D_top;
                    stepcounter = 2;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 9: {
                if(dLock != D_left){
                    direction = D_left;
                    dLock = D_right;
                    stepcounter = 3;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 10: {
                if(dLock != D_right){
                    direction = D_right;
                    dLock = D_left;
                    stepcounter = 3;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 11: {
                if(dLock != D_top){
                    direction = D_top;
                    dLock = D_down;
                    stepcounter = 3;
                }else {
                    dLock = D_random;
                }
                break;
            }
            case 12: {
                if(dLock != D_down){
                    direction = D_down;
                    dLock = D_top;
                    stepcounter = 3;
                }else {
                    dLock = D_random;
                }
                break;
            }
        }
    }


    switch(direction){
        case D_left:{
            iX = getPosition()->getPositionX() - 1;
            stepcounter--;
            break;
        }
        case D_right: {
            iX = getPosition()->getPositionX() + 1;
            stepcounter--;
            break;
        }
        case D_top: {
            iY = getPosition()->getPositionY() + 1;
            stepcounter--;
            break;
        }
        case D_down: {
            iY = getPosition()->getPositionY() - 1;
            stepcounter--;
            break;
        }

    }

    if((-1<iX && iX<10) && (-1<iY && iY<10)){
        setPosition(iX,iY);
        cout << "Moved thief to : "<< iX<<","<<iY << "\n";
        xRetVal = true;
    } else {
        stepcounter = 0;
        xRetVal = false;
    }

    return xRetVal;
}


