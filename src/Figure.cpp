#include "Figure.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

Figure::Figure()
{
    this->position = new Position();
    //ctor
}

Figure::~Figure()
{
    //dtor
}

void Figure::setPosition(unsigned int x,unsigned int y)
{
    this->position->setPositionX(x);
    this->position->setPositionY(y);
}

void Figure::setPosition(Position* positon)
{
    this->position = position;

}

Position* Figure::getPosition(){
    return this->position;
}

void Figure::randomPosition(){
    //random X,Y coordinates between 1-9
    int value;
    //srand(time(NULL));
    value = rand() % 9 + 1; //Number between 1 - 9
    this->position->setPositionX(value);

    value = rand() % 9 +1;
    this->position->setPositionY(value);
}

void Figure::setName(string name)
{
    this->name = name;
}

string Figure::getName()
{
    return this->name;
}
