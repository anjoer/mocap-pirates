#include "World.h"

World::World()
{
    this->initFieldMap();
    //ctor
}

World::~World()
{
    //dtor
}

void World::initFieldMap(){
    fieldmap[0][0] = sz_Surfing;
    fieldmap[1][0] = sz_Gitter;
    fieldmap[2][0] = sz_Geist;
    fieldmap[3][0] = sz_market;
    fieldmap[4][0] = sz_Lookout;
    fieldmap[5][0] = sz_Angeln;
    fieldmap[6][0] = sz_Handstand;
    fieldmap[7][0] = sz_Skipper;
    fieldmap[8][0] = sz_Kapitaen;
    fieldmap[9][0] = sz_Mastclimb;


    fieldmap[0][1] = sz_Dance;
    fieldmap[1][1] = sz_drunk;
    fieldmap[2][1] = sz_Ruehren;
    fieldmap[3][1] = sz_Dance;
    fieldmap[4][1] = sz_Saw;
    fieldmap[5][1] = sz_Shovel;
    fieldmap[6][1] = sz_drunk;
    fieldmap[7][1] = sz_Geist;
    fieldmap[8][1] = sz_Surfing;
    fieldmap[9][1] = sz_Surfing;

    fieldmap[0][2] = sz_Marschieren;
    fieldmap[1][2] = sz_Swordfight;
    fieldmap[2][2] = sz_Geist;
    fieldmap[3][2] = sz_OneDrunken;
    fieldmap[4][2] = sz_Hop;
    fieldmap[5][2] = sz_fight;
    fieldmap[6][2] = sz_cannon;
    fieldmap[7][2] = sz_fight;
    fieldmap[8][2] = sz_Angeln;
    fieldmap[9][2] = sz_Skipper;

    fieldmap[0][3] = sz_Marschieren;
    fieldmap[1][3] = sz_market;
    fieldmap[2][3] = sz_drunk;
    fieldmap[3][3] = sz_Swordfight;
    fieldmap[4][3] = sz_Selfkill;
    fieldmap[5][3] = sz_Saw;
    fieldmap[6][3] = sz_OneDrunken;
    fieldmap[7][3] = sz_Prostitute;
    fieldmap[8][3] = sz_Gitter;
    fieldmap[9][3] = sz_Mastclimb;

    fieldmap[0][4] = sz_Lookout;
    fieldmap[1][4] = sz_OneDrunken;
    fieldmap[2][4] = sz_Prostitute;
    fieldmap[3][4] = sz_Prostitute;
    fieldmap[4][4] = sz_Hop;
    fieldmap[5][4] = sz_Marschieren;
    fieldmap[6][4] = sz_Geist;
    fieldmap[7][4] = sz_market;
    fieldmap[8][4] = sz_Swordfight;
    fieldmap[9][4] = sz_Marschieren;

    fieldmap[0][5] = sz_Angeln;
    fieldmap[1][5] = sz_Shovel;
    fieldmap[2][5] = sz_fight;
    fieldmap[3][5] = sz_Hop;
    fieldmap[4][5] = sz_Hop;
    fieldmap[5][5] = sz_Shovel;
    fieldmap[6][5] = sz_Ruehren;
    fieldmap[7][5] = sz_market;
    fieldmap[8][5] = sz_drunk;
    fieldmap[9][5] = sz_Dance;

    fieldmap[0][6] = sz_Angeln;
    fieldmap[1][6] = sz_cannon;
    fieldmap[2][6] = sz_market;
    fieldmap[3][6] = sz_Saw;
    fieldmap[4][6] = sz_Selfkill;
    fieldmap[5][6] = sz_Shovel;
    fieldmap[6][6] = sz_Dance;
    fieldmap[7][6] = sz_Lookout;
    fieldmap[8][6] = sz_Prostitute;
    fieldmap[9][6] = sz_Handstand;

    fieldmap[0][7] = sz_Handstand;
    fieldmap[1][7] = sz_OneDrunken;
    fieldmap[2][7] = sz_fight;
    fieldmap[3][7] = sz_Gitter;
    fieldmap[4][7] = sz_Geist;
    fieldmap[5][7] = sz_Saw;
    fieldmap[6][7] = sz_Hop;
    fieldmap[7][7] = sz_fight;
    fieldmap[8][7] = sz_Hop;
    fieldmap[9][7] = sz_Skipper;

    fieldmap[0][8] = sz_Dance;
    fieldmap[1][8] = sz_Swordfight;
    fieldmap[2][8] = sz_Ruehren;
    fieldmap[3][8] = sz_Gitter;
    fieldmap[4][8] = sz_fight;
    fieldmap[5][8] = sz_Prostitute;
    fieldmap[6][8] = sz_Saw;
    fieldmap[7][8] = sz_Selfkill;
    fieldmap[8][8] = sz_cannon;
    fieldmap[9][8] = sz_Mastclimb;

    fieldmap[0][9] = sz_Surfing;
    fieldmap[1][9] = sz_drunk;
    fieldmap[2][9] = sz_Angeln;
    fieldmap[3][9] = sz_market;
    fieldmap[4][9] = sz_Angeln;
    fieldmap[5][9] = sz_Marschieren;
    fieldmap[6][9] = sz_Marschieren;
    fieldmap[7][9] = sz_drunk;
    fieldmap[8][9] = sz_OneDrunken;
    fieldmap[9][9] = sz_Kapitaen;
}

LVector3f World::getWorldCoordinates(){
    return 0;
}

SceneNames World::getSceneName(int p_iX,int p_iY){
    return this->fieldmap[p_iX][p_iY];
}
