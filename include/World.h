#ifndef WORLD_H
#define WORLD_H

#include <string.h>
#include "lvector3.h"
#include "Enums.h"

class World
{
    public:
        World();
        virtual ~World();
        void initFieldMap();
        LVector3f getWorldCoordinates();
        SceneNames getSceneName(int p_iX, int p_iY);
    protected:
    private:
        SceneNames fieldmap[10][10];
        LVector3f m_vCoordinates[10][10];

};

#endif // WORLD_H
