#ifndef THIEF_H
#define THIEF_H
#include <iostream>
using namespace std;
#include <Figure.h>
#include "Enums.h"

class Thief : public Figure
{
    public:
        Thief();
        virtual ~Thief();
        int getStepCounter() { return stepcounter; }
        void setStepCounter(int val) { stepcounter = val; }
        bool moveRandom();
    protected:
    private:
        int stepcounter;
        ThiefDirection direction;
        ThiefDirection dLock;
};

#endif // THIEF_H


