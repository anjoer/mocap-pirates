#ifndef GAME_H
#define GAME_H

//C-Core libraries
#include <map>
#include <string>
#include <vector>
using namespace std;

//Panda3D libraries
#include "pandaFramework.h"
#include "pandaSystem.h"
#include "cActor.h"
#include "cOnscreenText.h"

//game libraries
#include "Enums.h"
#include "Dice.h"
#include "Thief.h"
#include "Player.h"
#include "World.h"
#include "Scene.h"
#include "Scene_drunk.h"
#include "Scene_fight.h"
#include "Scene_market.h"
#include "Scene_oneDrunken.h"
#include "Scene_Gitter.h"
#include "Scene_kapitaen.h"
#include "Scene_marschieren.h"
#include "Scene_ruehren.h"
#include "Scene_geist.h"
#include "Scene_sandburg.h"
#include "Scene_clap.h"
#include "Scene_skipper.h"
#include "Scene_surfing.h"
#include "Scene_mastclimb.h"
#include "Scene_hop.h"
#include "Scene_dance.h"
#include "Scene_lookout.h"
#include "Scene_shovel.h"
#include "Scene_saw.h"
#include "Scene_swordfight.h"
#include "Scene_prostitute.h"
#include "Scene_selfkill.h"
#include "Scene_handstand.h"
#include "Scene_cannon.h"
#include "Scene_angeln.h"



class Game
{
    CActor ca;
    public:
        Game(WindowFramework* p_ptrWindowFramework);
        virtual ~Game();
    protected:
    private:
        PT(WindowFramework) m_ptrWindowFramework;
        World* m_ptrWorld = new World();
        Dice* m_ptrDice = new Dice();
        Thief* m_ptrThief = new Thief();
        Player* m_ptrPlayer = new Player();
        NodePath m_Camera;
        NodePath m_Environment;
        NodePath m_Water;

        NodePath m_ptrRenderNp;
        NodePath m_ModelsNp;
        NodePath m_PlayerModel;
        NodePath m_ThiefModel;
        COnscreenText* m_ptrInfoText;
        COnscreenText* m_ptrMenuText;
        COnscreenText* m_ptrTextBox;
        // define globals Strings
        string m_sMenuText = "Menu: \n\n     (s) Start New Game \n\n     (i) How To Play This Game\n\n\     (c) Credits\n\n\n     [esc] Quit Game";
        string m_sCredits = "Credits:\n\nDV-Gruppe:\nKarin Weiland, Andreas Allgaier, Felix Popp, Werner Deuring, Andreas Jörg\n\nVielen Dank an die Schauspieler von \"Blickkontakt\":\nKonstantin, Jochen, Jasmin, Steffi, Joana, Nicole, Michi, Patrick, Amelie, Babs, Nadine, ...\n\n (b) back";
        string m_sHowToPlay = "Finde den Dieb und rette die Ehre der Piraten Insel.\n\nWie der Titel des Spiels schon vermuten lässt, befinden wir uns auf einer Piraten Insel inmitten der Südsee. Doch etwas Schreckliches ist passiert. Der Schatz des Piraten Kapitäns Felix wurde von einem dreisten Dieb gestohlen und ist nun in dessen Besitz. Glücklicherweise war der magische Ring der Meernymphe Thalia auch in der Schatzkiste. Kapitän Felix gewann den Ring beim letzten Schwertfischrennen in der schwarzen Bucht. Der Ring hat die Eigenschaft die Umgebung seines Besitzers in kleinen Computerszenen zu animieren und diese auf magische Weise an andere Talismane zu übertragen. Zufälligerweise ist Kapitän Felix im Besitz eines solchen Talismans, nämlich dem Amulett von Nesaia, der Schwester Thalias.Kapitän Felix überreicht Euch das Amulett von Nesaia und schickt Euch los, um den Dieb zu fassen und den gestohlenen Schatz wieder zurück zu bringen. Der Dieb scheint noch nicht den magischen Ring bemerkt zu haben, deshalb beeilt Euch, bevor der Dieb herausfindet, dass Ihr ihm auf dem Fersen seid!\n\nDu kannst deine Spielfigur mit den Pfeiltasten nach oben, unten, links und rechts bewegen.\n\nFängst du den Dieb innerhalb von 10 Spielrunden, gewinnst du. Sonst gewinnt der Dieb.\n\nWir wünschen dir viel Spaß beim Spielen!! \n\n (b) back";

        int m_iRoundCounter;
        bool m_xGameOver;
        bool m_xSceneState;
        map<string,CActor> m_vActors;
        map<SceneNames,Scene*> m_vScenes; //contains pointers to scenes used in the game
        map<string,NodePath> m_vAssets; //contains all used static Models used in scenes
        vector<bool> m_vKeymap; //registers a key evenet and stores it as a bool value
        State m_eState;

        // loads egg-files, scales actors, positions actors, initializes m_vActors
        void setupActors();
        // loads egg-files, scales assets, positions assets, initializes m_vAssets
        void setupAssets();
        // initializes m_vScenes
        void setupScenes();
        // initializes m_vKeymap
        void setupKeyEvent();
        // setup text that appears during gameplay
        void setupOnScreenText();
        // checks if a move is valid
        bool checkValidMove(Player* p_ptrPlayer,int p_iX, int p_iY,int p_iDiceValue);
        // check if game is over
        bool checkEndOfGame(Player* p_ptrPlayer, Thief* p_ptrThief);
        // calls asyncronous task playGame
        static AsyncTask::DoneStatus callPlayGame(GenericAsyncTask* p_ptrTask, void* p_ptrData);
        // main function that runs the game
        void playGame();
        // set camera for world
        void setCamToWorld();
        // set camera for stage
        void setCamToStage();
        // calls setKey if key event occurs
        //vector<bool> m_keyMap;
        template<int iKey,bool xValue> static void callSetKey(const Event* p_ptrEvent,void* p_ptrData);
        // registers a pressed key as bool value in m_vKeymap
        void setKey(Key p_eKey,bool p_xValue);
        // checks for exit event
        static void sys_exit(const Event* p_ptrEvent, void* p_ptrData);

};

#endif // GAME_H
