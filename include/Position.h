#ifndef POSITION_H
#define POSITION_H


class Position
{
    public:
        Position();
        Position(int x, int y);
        virtual ~Position();
        int getPositionX() { return positionX; }
        void setPositionX(unsigned int x) {this->positionX = x;}
        int getPositionY() { return positionY; }
        void setPositionY(unsigned int y) { this->positionY = y; }
        int getPositionValue();
        void setPosition(unsigned int x, unsigned int y);
    protected:
    private:
        int positionX;
        int positionY;
};

#endif // POSITION_H
