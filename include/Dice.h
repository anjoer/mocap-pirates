#ifndef DICE_H
#define DICE_H


class Dice
{
    public:
        Dice();
        virtual ~Dice();
        unsigned int throwDice();
        int value;
        int getValue();
        bool decrement();
    protected:
    private:
};

#endif // DICE_H
