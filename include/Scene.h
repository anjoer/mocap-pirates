#ifndef SCENE_H
#define SCENE_H
#include <string>
#include <iostream>
using namespace std;
#include <stdlib.h>
#include <map>
#include "cActor.h"


class Scene
{
    public:
        Scene(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets, NodePath* p_ptrRenderNp);
        Scene();
        virtual ~Scene();
        virtual bool playScene() = 0;
        virtual bool isPlaying() = 0;
        virtual bool stopScene() = 0;
        virtual bool isDone() = 0;
        virtual bool isRdy() = 0;
        void setName(string sname){this->m_sname = sname;}
        string getName(){return m_sname;}
    protected:
        map<string,CActor>* m_ptrActors;
        map<string,NodePath>* m_ptrAssets;
        string m_sname;
        bool m_xIsPlaying;
        bool m_xIsDone;
        bool m_xIsRdy;
        NodePath* m_renderNp = new NodePath();
    private:


};

#endif // SCENE_H

