#ifndef FIGURE_H
#define FIGURE_H

#include "Position.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <string>
using namespace std;


class Figure
{
    public:
        Figure();
        virtual ~Figure();
        void setPosition(unsigned int x,unsigned int y);
        void randomPosition();
        void setPosition(Position* position);
        Position* getPosition();
        void setName(string name);
        string getName();
    protected:
    private:
        unsigned int id;
        string name;
        Position* position;
};

#endif // FIGURE_H
