#ifndef SCENE_MARSCHIEREN_H
#define SCENE_MARSCHIEREN_H

#include <Scene.h>


class Scene_marschieren : public Scene
{
    public:
        Scene_marschieren(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_marschieren();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};
#endif // SCENE_MARSCHIEREN_H
