#ifndef SCENE_ONEDRUNKEN_H
#define SCENE_ONEDRUNKEN_H

#include <Scene.h>


class Scene_oneDrunken : public Scene
{
    public:
        Scene_oneDrunken(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_oneDrunken();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:

};
#endif // SCENE_ONEDRUNKEN_H
