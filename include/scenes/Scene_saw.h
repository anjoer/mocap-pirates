#ifndef SCENE_SAW_H
#define SCENE_SAW_H

#include <Scene.h>


class Scene_saw : public Scene
{
    public:
        Scene_saw(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        ~Scene_saw();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_SAW_H
