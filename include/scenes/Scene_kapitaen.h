#ifndef SCENE_KAPITAEN_H
#define SCENE_KAPITAEN_H

#include <Scene.h>


class Scene_kapitaen : public Scene
{
    public:
        Scene_kapitaen(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_kapitaen();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};
#endif // SCENE_KAPITAEN_H
