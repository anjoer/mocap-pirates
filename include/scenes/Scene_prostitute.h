#ifndef SCENE_PROSTITUTE_H
#define SCENE_PROSTITUTE_H

#include <Scene.h>


class Scene_prostitute : public Scene
{
    public:
        Scene_prostitute(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        ~Scene_prostitute();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_PROSTITUTE_H
