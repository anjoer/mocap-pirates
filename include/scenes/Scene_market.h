#ifndef SCENE_MARKET_H
#define SCENE_MARKET_H

#include <Scene.h>


class Scene_market : public Scene
{
    public:
        Scene_market(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_market();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_MARKET_H
