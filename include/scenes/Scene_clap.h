#ifndef SCENE_CLAP_H
#define SCENE_CLAP_H

#include <Scene.h>


class Scene_clap : public Scene
{
    public:
        Scene_clap(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_clap();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_CLAP_H
