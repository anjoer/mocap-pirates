#ifndef SCENE_SELFKILL_H
#define SCENE_SELFKILL_H

#include <Scene.h>


class Scene_selfkill : public Scene
{
    public:
        Scene_selfkill(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        ~Scene_selfkill();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_SELFKILL_H
