#ifndef SCENE_SKIPPER_H
#define SCENE_SKIPPER_H

#include <Scene.h>


class Scene_skipper : public Scene
{
    public:
        Scene_skipper(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_skipper();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_SKIPPER_H
