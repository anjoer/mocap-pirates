#ifndef SCENE_SWORDFIGHT_H
#define SCENE_SWORDFIGHT_H

#include <Scene.h>


class Scene_swordfight : public Scene
{
    public:
        Scene_swordfight(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        ~Scene_swordfight();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_SWORDFIGHT_H
