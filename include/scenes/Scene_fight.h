#ifndef SCENE_FIGHT_H
#define SCENE_FIGHT_H

#include <Scene.h>


class Scene_fight : public Scene
{
    public:
        Scene_fight(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_fight();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_FIGHT_H
