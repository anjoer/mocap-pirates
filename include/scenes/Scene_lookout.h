#ifndef SCENE_LOOKOUT_H
#define SCENE_LOOKOUT_H

#include <Scene.h>


class Scene_lookout : public Scene
{
    public:
        Scene_lookout(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        ~Scene_lookout();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_LOOKOUT_H
