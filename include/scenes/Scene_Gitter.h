#ifndef SCENE_GITTER_H
#define SCENE_GITTER_H

#include <Scene.h>


class Scene_Gitter : public Scene
{
    public:
        Scene_Gitter(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_Gitter();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_GITTER_H
