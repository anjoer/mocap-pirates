#ifndef SCENE_RUEHREN_H
#define SCENE_RUEHREN_H

#include <Scene.h>


class Scene_ruehren : public Scene
{
    public:
        Scene_ruehren(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_ruehren();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_RUEHREN_H
