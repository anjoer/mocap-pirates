#ifndef SCENE_CANNON_H
#define SCENE_CANNON_H

#include <Scene.h>


class Scene_cannon : public Scene
{
    public:
        Scene_cannon(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        ~Scene_cannon();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_CANNON_H
