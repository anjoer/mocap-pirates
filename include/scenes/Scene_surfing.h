#ifndef SCENE_SURFING_H
#define SCENE_SURFING_H

#include <Scene.h>


class Scene_surfing : public Scene
{
    public:
        Scene_surfing(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_surfing();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_SURFING_H
