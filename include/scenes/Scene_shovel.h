#ifndef SCENE_SHOVEL_H
#define SCENE_SHOVEL_H

#include <Scene.h>


class Scene_shovel : public Scene
{
    public:
        Scene_shovel(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        ~Scene_shovel();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_SHOVEL_H
