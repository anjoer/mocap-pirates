#ifndef SCENE_MASTCLIMBT_H
#define SCENE_MASTCLIMBT_H

#include <Scene.h>


class Scene_mastclimb : public Scene
{
    public:
        Scene_mastclimb(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_mastclimb();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_MASTCLIMBT_H
