#ifndef SCENE_DANCE_H
#define SCENE_DANCE_H

#include <Scene.h>


class Scene_dance : public Scene
{
    public:
        Scene_dance(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_dance();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_DANCE_H
