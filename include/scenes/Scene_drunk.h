#ifndef SCENE_DRUNK_H
#define SCENE_DRUNK_H

#include <Scene.h>


class Scene_drunk : public Scene
{
    public:
        Scene_drunk(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        ~Scene_drunk();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:



};

#endif // SCENE_DRUNK_H
