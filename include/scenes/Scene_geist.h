#ifndef SCENE_GEIST_H
#define SCENE_GEIST_H

#include <Scene.h>


class Scene_geist : public Scene
{
    public:
        Scene_geist(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_geist();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_GEIST_H
