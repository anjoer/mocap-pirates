#ifndef SCENE_ANGELN_H
#define SCENE_ANGELN_H

#include <Scene.h>


class Scene_angeln : public Scene
{
    public:
        Scene_angeln(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_angeln();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};
#endif // SCENE_ANGELN_H
