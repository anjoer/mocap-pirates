#ifndef SCENE_SANDBURG_H
#define SCENE_SANDBURG_H

#include <Scene.h>


class Scene_sandburg : public Scene
{
    public:
        Scene_sandburg(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_sandburg();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_SANDBURG_H
