#ifndef SCENE_HANDSTAND_H
#define SCENE_HANDSTAND_H

#include <Scene.h>


class Scene_handstand : public Scene
{
    public:
        Scene_handstand(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        ~Scene_handstand();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_HANDSTAND_H
