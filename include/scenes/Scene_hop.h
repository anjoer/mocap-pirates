#ifndef SCENE_HOP_H
#define SCENE_HOP_H

#include <Scene.h>


class Scene_hop : public Scene
{
    public:
        Scene_hop(map<string,CActor>* p_ptrActors,map<string,NodePath>* p_ptrAssets,NodePath* p_ptrRenderNp);
        virtual ~Scene_hop();
        bool playScene();
        bool isPlaying();
        bool stopScene();
        bool isDone();
        bool isRdy();
    protected:
    private:
};

#endif // SCENE_HOP_H
