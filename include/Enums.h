#ifndef ENUMS_H_INCLUDED
#define ENUMS_H_INCLUDED

enum SceneNames
{
    sz_drunk = 0,
    sz_cannon,
    sz_fight,
    sz_market,
    sz_OneDrunken,
    sz_Gitter,
    sz_Kapitaen,
    sz_Marschieren,
    sz_Ruehren,
    sz_Geist,
    sz_Sandburg,
    sz_Clap,
    sz_Skipper,
    sz_Surfing,
    sz_Mastclimb,
    sz_Hop,
    sz_Dance,
    sz_Lookout,
    sz_Shovel,
    sz_Saw,
    sz_Swordfight,
    sz_Prostitute,
    sz_Selfkill,
    sz_Handstand,
    sz_Schmied,
    sz_Angeln

};
enum Key
{
    K_left = 0,
    K_right,
    K_forward,
    K_down,
    K_s_startGame,
    K_i_info,
    K_c_credits,
    K_b_back,
    K_EndKey
};

enum State
{
    MENU = 0,
    NEWGAME,
    MOVETHIEF,
    CANNON,
    PLAYSCENE,
    DICE,
    RESET_KEYMAP,
    MOVEPLAYER,
    GAMEOVER

};

enum ThiefDirection {
    D_left,
    D_right,
    D_top,
    D_down,
    D_random
};

#endif // ENUMS_H_INCLUDED
